const https = require('https');
const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const options = {
  pfx: fs.readFileSync('./gis.ingrad.com.pfx'),
  passphrase: '123456'
};

const app = express();
const server = https.createServer(options, app);

const port = process.env.PORT || 9092;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'build')));
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});
app.get('*', (req, res) => res.sendFile(path.join(__dirname, 'build', 'index.html')));

server.listen(port, () => console.log('start on ' + port));
