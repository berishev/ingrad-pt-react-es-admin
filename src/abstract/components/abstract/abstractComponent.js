import * as React from 'react';
import { LINQ } from 'berish-linq';
import * as accessors from 'berish-accessors-string';
import { promiseGet } from 'berish-promise-getter';
export class AbstractComponent extends React.Component {
    getConfig(attribute, props) {
        props = props || this.props;
        let configProps = props.config;
        let configsProps = props.configs;
        let config = null;
        if (attribute == null) {
            if (configsProps) {
                let linq = LINQ.fromArray(configsProps || []);
                let count = linq.count();
                if (count == 1)
                    config = configProps[0];
                else {
                    config = linq.single(m => m.type == null);
                }
            }
            else {
                config = configProps;
            }
        }
        else {
            if (configsProps) {
                let linq = LINQ.fromArray(configsProps || []);
                config = linq.single(m => m.type == attribute);
            }
            else {
                throw new Error('you trying to get value by attribute - index of Array, but got Object in AbstractComponent');
            }
        }
        return config || {};
    }
    get config() {
        try {
            return this.getConfig();
        }
        catch (err) {
            return null;
        }
    }
    async setValue(value, attrName, props) {
        let config = this.getConfig(attrName, props);
        if (config.beforeSet)
            value = await promiseGet(config.beforeSet(config.object, config.path, value));
        accessors.setValue(config.object, config.path, value);
        if (config.afterSet)
            await promiseGet(config.afterSet(config.object, config.path, value));
    }
    getValue(attrName, props) {
        let config = this.getConfig(attrName, props);
        if (config.beforeGet)
            config.beforeGet(config.object, config.path);
        let value = accessors.getValue(config.object, config.path);
        if (config.afterGet)
            value = config.afterGet(config.object, config.path, value);
        return value;
    }
}
