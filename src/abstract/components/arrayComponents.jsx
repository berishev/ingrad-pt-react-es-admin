import * as React from 'react';
import { LINQ } from 'berish-linq';
function getLINQ(linqOrArray) {
    if (linqOrArray instanceof LINQ) {
        return linqOrArray;
    }
    return LINQ.fromArray(linqOrArray || []);
}
export default class ArrayComponents extends React.Component {
    static render(props) {
        return this.renderTemplates(props);
    }
    static async renderAsync(props) {
        return this.renderTemplatesAsync(props);
    }
    static renderTemplates(props) {
        let { elements, template, args } = props;
        return getLINQ(elements)
            .selectMany((m, i) => {
            let item = template(i, m, ...(args || []));
            return item instanceof Array ? item : [item];
        })
            .toArray();
    }
    static async renderTemplatesAsync(props) {
        let { elements, template, args } = props;
        let temp = await Promise.all(getLINQ(elements)
            .select(async (m, i) => {
            let item = await template(i, m, ...(args || []));
            return item instanceof Array ? item : [item];
        })
            .toArray());
        return getLINQ(temp)
            .selectMany(m => m)
            .toArray();
    }
    render() {
        let { parent } = this.props;
        let renderData = ArrayComponents.render(this.props);
        if (parent)
            return React.cloneElement(parent, {}, renderData);
        else {
            return <div>{renderData}</div>;
        }
    }
    renderTemplates() {
        return ArrayComponents.renderTemplates(this.props);
    }
}
