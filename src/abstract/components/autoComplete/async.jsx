import * as React from 'react';
import View from './view';
import { AbstractComponent } from '../abstract';
import { LINQ } from 'berish-linq';
export default class AutoCompleteAsync extends AbstractComponent {
    constructor(props) {
        super(props);
        this.onFetch = async (text) => {
            let data = await this.props.onFetch(text);
            this.setState({ data });
        };
        this.onChange = (text) => {
            if (this.props.onChange)
                this.props.onChange(text);
            if (this.props.onSelect) {
                let item = LINQ.fromArray(this.state.data).firstOrNull(m => m.text == text);
                if (item)
                    this.props.onSelect(text, item.value);
            }
        };
        this.state = {
            data: [],
            loading: false
        };
    }
    render() {
        return (<View {...this.props} data={this.state.data.map(m => m.text)} onFetch={this.onFetch} onChange={this.onChange}/>);
    }
}
