import * as React from 'react';
import { Button as AButton } from '@material-ui/core';
import { LINQ } from 'berish-linq/dist';
import Decorators from '../util/decorators';
import { Tooltip, FormItem } from './abstract';
import messagesController from '../global/messagesController';
class Button extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = event => {
            let onClick = this.props.onClick;
            if (onClick)
                onClick(event);
        };
    }
    getValidations(objects) {
        if (!objects) {
            return false;
        }
        if (objects instanceof Array) {
            if (objects.length <= 0) {
                return [];
            }
            return LINQ.fromArray(objects)
                .selectMany(m => (m instanceof Array ? m : [m]))
                .notNull()
                .toArray();
        }
        else {
            return [objects];
        }
    }
    render() {
        let { buttonTitle, ...props } = this.props;
        let style = Object.assign({}, this.props.shape ? null : { width: '100%' }, this.props.style || {});
        let input = (<AButton {...props} style={style} size={this.props.size || 'large'} disabled={this.props.disabled} color={this.props.type} variant={this.props.variant} onClick={event => {
            let submit = this.props.submit || {};
            let validationObject = this.getValidations(submit.validationObject);
            if (validationObject == false) {
                this.onClick(event);
            }
            else {
                let filters = LINQ.fromArray(validationObject || []).selectMany(m => Decorators.Filter.Validate(m).toArray());
                let error = filters.any(m => m.isError);
                if (error) {
                    for (let model of filters.where(m => m.isError).toArray()) {
                        model.isSetExecute = true;
                    }
                    {
                        messagesController.notification({
                            type: 'error',
                            message: 'Заполните все поля корректно'
                        });
                    }
                    if (submit.controller)
                        submit.controller.forceUpdate();
                }
                else {
                    this.onClick(event);
                }
            }
        }}>
        {this.props.buttonTitle || this.props.fiTitle}
        {this.props.icon}
      </AButton>);
        return Tooltip(input, {
            placeholder: this.props.placeholder || this.props.buttonTitle || this.props.fiTitle,
            trigger: 'hover',
            disableTooltip: this.props.disableTooltip,
            isButton: true
        });
    }
}
export default FormItem(Button, { fiDisable: true });
