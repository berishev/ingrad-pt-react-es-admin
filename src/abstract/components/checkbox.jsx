import * as React from 'react';
import { Checkbox as MCheckbox } from '@material-ui/core';
import { AbstractComponent, Tooltip, FormItem } from './abstract';
class Checkbox extends AbstractComponent {
    constructor(props) {
        super(props);
        this.onChange = (value) => {
            if (this.props.onChange)
                this.props.onChange(value);
            this.setValue(value);
        };
    }
    render() {
        let value = typeof this.props.value == 'undefined' ? this.getValue() : this.props.value;
        let placeholder = this.props.placeholder || this.props.fiTitle;
        let input = (<MCheckbox disabled={this.props.disabled} checked={value} onChange={e => this.onChange(e.target.checked)}/>);
        return this.props.disabledTooltip
            ? input
            : Tooltip(input, {
                placeholder: placeholder,
                trigger: 'hover'
            });
    }
}
export default FormItem(Checkbox, { fiTitleType: 'FormControlLabel' });
