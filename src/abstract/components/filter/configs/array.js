import AbstractConfig from '../config';
export default class ArrayAbstractConfig extends AbstractConfig {
    applyMethod() {
        let result = super.applyMethod();
        let linq = this.getLinq(result);
        if (this.filters != null && this.filters.length > 0) {
            let filters = this.getLinq(this.filters);
            linq = linq.where(m => {
                return filters.all(k => k(m));
            });
        }
        return linq.toArray();
    }
}
