import * as React from 'react';
import Divider from './divider';
export default class extends React.Component {
    constructor() {
        super(...arguments);
        this.renderWrapper = (content) => React.Children.count(content) || this.props.style ? (<div style={{ display: 'flex', flexDirection: 'row', flexWrap: this.props.wrap || 'nowrap', ...this.props.style }}>
        {content}
      </div>) : (content);
    }
    render() {
        return (<>
        {this.props.divider && <Divider>{this.props.divider}</Divider>}
        {this.renderWrapper(this.props.children)}
      </>);
    }
}
