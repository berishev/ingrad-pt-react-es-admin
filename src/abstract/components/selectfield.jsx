import * as React from 'react';
import { Select, MenuItem, Input } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { LINQ } from 'berish-linq/dist';
import { AbstractComponent, Tooltip, FormItem } from './abstract';
import ArrayComponents from './arrayComponents';
export function isLinq(data) {
    return data instanceof LINQ;
}
export function getLinq(data) {
    if (isLinq(data))
        return data;
    return LINQ.fromArray(data || []);
}
class SelectField extends AbstractComponent {
    constructor(props) {
        super(props);
        this.onChange = (key) => {
            let value = this.convertValue(key);
            this.setValue(value && value.value);
        };
        this.renderOption = (index, value) => {
            return (<MenuItem key={index} value={`${index}`}>
        {value.view}
      </MenuItem>);
        };
        this.convertValue = (value) => {
            let linq = getLinq(this.props.data);
            return value && linq.toArray()[parseInt(value, 10) || 0];
        };
        this.renderValue = (linq) => {
            let { select } = this.props;
            let value = this.getValue();
            if (value == null)
                return null;
            let linqData = linq.select(m => m.value);
            if (select == null &&
                linq.all(m => m.value && typeof m.value == 'object' && 'id' in m.value)) {
                select = data => {
                    return data['id'];
                };
            }
            if (select != null) {
                value = select(value);
                linqData = linqData.select(select);
            }
            let index = linqData.toArray().indexOf(value);
            if (index == -1)
                return null;
            return `${index}`;
        };
        this.renderSuffix = (value) => {
            return value ? <Close onClick={this.clearValue}/> : null;
        };
        this.clearValue = () => {
            this.onChange(null);
        };
    }
    render() {
        let linq = getLinq(this.props.data);
        let value = this.renderValue(linq);
        const input = (<Select value={value != null ? value : []} onChange={e => this.onChange(e.target.value)} disabled={this.props.disabled} fullWidth={true} placeholder={this.props.placeholder} input={<Input />} endAdornment={!!this.props.disabled ? undefined : this.renderSuffix(value)}>
        {ArrayComponents.render({
            template: this.renderOption,
            elements: linq
        })}
      </Select>);
        return Tooltip(input, {
            placeholder: this.props.placeholder,
            trigger: 'focus'
        });
    }
}
export default FormItem(SelectField, { fiTitleType: 'InputLabel' });
