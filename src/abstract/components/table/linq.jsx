import AbstractTable, { ON_PAGE_DEFAULT } from './abstract';
import { LINQ } from 'berish-linq/dist';
function getLinq(linqArray) {
    if (linqArray instanceof LINQ)
        return linqArray;
    return LINQ.fromArray(linqArray || []);
}
export default class LINQTable extends AbstractTable {
    constructor(props) {
        super(props);
        this.load = async (nextProps) => {
            let { data } = nextProps || this.props;
            if (data == null)
                return;
            let { allData, showData, total, currentPage } = this.state;
            currentPage = 1;
            let temp = await data;
            allData = getLinq(temp);
            total = allData.count();
            showData = await this.loadPage(currentPage, nextProps, null, {
                ...this.state,
                allData
            });
            this.setState({
                allData,
                showData,
                total,
                currentPage
            });
        };
        this.loadPage = async (page, nextProps, pageSize, state) => {
            let { onPage } = nextProps || this.props;
            let { showData } = this.state;
            let limit = pageSize || onPage || ON_PAGE_DEFAULT;
            let queryData = state.allData.skip(limit * (page - 1)).take(limit);
            showData = queryData.toArray();
            return showData;
        };
        this.sortPage = async (column, type) => {
            let { allData, showData, currentPage } = this.state;
            let renders = allData.select((m, i) => ({
                render: column.sorter ? column.sorter(m, i) : column.render(m, i),
                data: m
            }));
            if (type == 'down')
                renders = renders.orderBy(m => m.render);
            else if (type == 'up')
                renders = renders.orderByDescending(m => m.render);
            allData = renders.select(m => m.data);
            showData = await this.loadPage(currentPage, null, null, {
                ...this.state,
                allData
            });
            this.setState({ allData, showData });
        };
        this.state = {
            ...this.state,
            allData: LINQ.fromArray([])
        };
    }
}
