import * as React from 'react';
import { Select, MenuItem, Input, Icon, Chip } from '@material-ui/core';
import { LINQ } from 'berish-linq/dist';
import { AbstractComponent, Tooltip, FormItem } from './abstract';
import ArrayComponents from './arrayComponents';
import { getLinq } from './selectfield';
class Tagsfield extends AbstractComponent {
    constructor(props) {
        super(props);
        this.onChange = (key) => {
            let data = this.convertValue(key, true);
            if (data instanceof Array) {
                let linq = LINQ.fromArray(data)
                    .select(m => m.value)
                    .toArray();
                this.setValue(linq);
            }
        };
        this.renderOption = (index, value) => {
            return (<MenuItem key={index} value={`${index}`}>
        {value.view}
      </MenuItem>);
        };
        this.renderChip = (index, value) => {
            return <Chip key={`${index}`} label={value.view}/>;
        };
        this.renderValue = (linq) => {
            let { select } = this.props;
            let value = this.getValue();
            if (value == null)
                return null;
            let linqData = linq.select(m => m.value);
            if (select == null && linq.all(m => m.value && typeof m.value == 'object' && 'id' in m.value)) {
                select = data => {
                    return data['id'];
                };
            }
            if (select != null) {
                value = LINQ.fromArray(value)
                    .select(m => select(m))
                    .toArray();
                linqData = linqData.select(select);
            }
            let indexes = LINQ.fromArray(value)
                .select(m => linqData.toArray().indexOf(m))
                .where(m => m != -1)
                .select(m => `${m}`)
                .toArray();
            return indexes;
        };
        this.renderSuffix = (value) => {
            return value && Array.isArray(value) && value.length > 0 ? (<Icon onClick={this.clearValue}>close-circle</Icon>) : null;
        };
        this.clearValue = () => {
            this.onChange([]);
        };
    }
    convertValue(value, isOnChange) {
        let linq = getLinq(this.props.data);
        let array = linq.toArray();
        let rdy = LINQ.fromArray(value)
            .select(m => {
            let parsed = parseInt(m, 10);
            if (isOnChange) {
                if (Number.isNaN(parsed))
                    return m;
            }
            return array[parsed || 0];
        })
            .notNull();
        let isStringAny = rdy.any(m => typeof m == 'string');
        if (isStringAny && isOnChange)
            return {
                type: 'string',
                data: rdy.where(m => typeof m == 'string').toArray()
            };
        return rdy.toArray();
    }
    render() {
        let linq = getLinq(this.props.data);
        let value = this.renderValue(linq);
        const input = (<Select multiple={true} value={value != null ? value : []} onChange={e => this.onChange(e.target.value)} disabled={this.props.disabled} fullWidth={true} placeholder={this.props.placeholder} input={<Input />} endAdornment={!!this.props.disabled ? undefined : this.renderSuffix(value)}>
        {ArrayComponents.render({
            template: this.renderOption,
            elements: linq
        })}
      </Select>);
        return Tooltip(input, {
            placeholder: this.props.placeholder,
            trigger: 'click'
        });
    }
}
export default FormItem(Tagsfield, { fiTitleType: 'InputLabel' });
