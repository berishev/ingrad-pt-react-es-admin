import * as React from 'react';
import { Modal } from '..';
export default class UploadModal extends React.Component {
    constructor(props) {
        super(props);
        this.resolve = () => {
            this.props.resolve();
        };
        this.reject = () => {
            this.resolve();
        };
    }
    render() {
        return (<Modal.Form resolve={this.resolve} reject={this.reject} width="40vw">
        <img style={{ width: '100%' }} src={this.props.url}/>
      </Modal.Form>);
    }
}
