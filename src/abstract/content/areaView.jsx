import * as React from 'react';
import { Helmet } from 'react-helmet';
import HeaderView from './headerView';
import ContentView from './contentView';
import MenuView from './menuView';
import { storageController, configController } from '../global';
import withStyles from './withStyles';
class AreaView extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { classes } = this.props;
        let { title } = storageController.systemStore;
        return (<div>
        <Helmet defaultTitle={configController.meta.projectName} titleTemplate={`${configController.meta.projectName} - %s`}>
          <title>{title}</title>
        </Helmet>
        <div className={classes.root}>
          <HeaderView {...this.props}/>
          <MenuView {...this.props}/>
          <ContentView {...this.props}/>
        </div>
      </div>);
    }
}
export default withStyles(AreaView);
