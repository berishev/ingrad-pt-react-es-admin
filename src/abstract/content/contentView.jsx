import * as React from 'react';
import withStyles from './withStyles';
class ContentView extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { classes } = this.props;
        return (<main className={classes.content}>
        <div className={classes.appBarSpacer}/>
        <div className={classes.contentContainer} style={{ overflowY: 'hidden' }}>
          <div style={{ height: '100%' }}>{this.props.children}</div>
        </div>
      </main>);
    }
}
export default withStyles(ContentView);
