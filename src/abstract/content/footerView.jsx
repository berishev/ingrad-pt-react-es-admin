import * as React from 'react';
import { configController } from '../global';
export default class FooterView extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (<div style={{
            textAlign: 'center'
        }}>
        {configController.meta.projectName} © {new Date().getFullYear()}
      </div>);
    }
}
