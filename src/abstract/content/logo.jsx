import * as React from 'react';
import { configController } from '../global';
export default class Logo extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        if (this.props.disableText)
            return (<div style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: this.props.justifyContent || 'center',
                alignItems: 'center',
                width: '100%',
                maxWidth: '550px',
                marginTop: '25px',
                marginBottom: '25px'
            }}>
          <img style={{
                height: this.props.logoHeight || 75,
                width: this.props.logoWidth
            }} src="/images/logo.png"/>
        </div>);
        return (<div style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: this.props.justifyContent || 'center',
            alignItems: 'center',
            width: '100%',
            maxWidth: '550px',
            marginTop: '25px',
            marginBottom: '25px'
        }}>
        <img style={{
            height: this.props.logoHeight || 75,
            width: this.props.logoWidth || 75
        }} src="/images/logo.png"/>
        <span style={{
            marginLeft: '25px',
            fontSize: this.props.fontSize || 50,
            fontFamily: 'Tahoma',
            color: 'red',
            textTransform: 'uppercase'
        }}>
          {configController.meta.logoText}
        </span>
      </div>);
    }
}
