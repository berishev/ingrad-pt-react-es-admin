import Portal from 'berish-react-portals';
import ModalRoute from '../router/modalRoute';
export class NavigatorController {
    constructor() {
        this._history = null;
        this._params = null;
    }
    static init(contentRoute, controller) {
        if (!controller)
            controller = new NavigatorController();
        return controller.receive(contentRoute);
    }
    receive(contentRoute) {
        this._history = contentRoute.history;
        this._params = contentRoute.params;
        return this;
    }
    get params() {
        return this._params || {};
    }
    get location() {
        return this._history.location;
    }
    prepareUrl(path, params) {
        let url = Object.keys(params || {})
            // .filter(key => params[key] != null)
            .map(key => `${key}=${params[key]}`)
            .join('&');
        return `${path}${url ? '?' + url : ''}`;
    }
    push(path, params) {
        let paramsUrl = this.prepareUrl(path, params);
        this._history.push(paramsUrl);
    }
    pushModal(component) {
        let decorate = Portal.create(ModalRoute);
        return (params) => {
            let props = {
                component,
                params,
                history: this._history
            };
            return decorate(props);
        };
    }
    goBack(path, params) {
        return this.push(path, params);
    }
    replace(path, params) {
        let paramsUrl = this.prepareUrl(path, params);
        this._history.replace(paramsUrl);
    }
}
