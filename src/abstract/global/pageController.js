import { NavigatorController } from './navigatorController';
export class PageController {
    constructor() {
        this._navigatorController = null;
    }
    static init(contentRoute, controller) {
        if (!controller)
            controller = new PageController();
        return controller.receive(contentRoute);
    }
    receive(router) {
        this._navigatorController = NavigatorController.init(router, this._navigatorController);
        return this;
    }
    get navigator() {
        return this._navigatorController;
    }
}
