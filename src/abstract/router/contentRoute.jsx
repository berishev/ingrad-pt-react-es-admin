import * as React from 'react';
import AreaView from '../content/areaView';
import { PageController } from '../global/pageController';
import { getNotUsedAreaView } from '../util/decorators/route';
import { storageController } from '../global';
export default class ContentRoute extends React.PureComponent {
    constructor(props) {
        super(props);
        this.systemUnlistener = storageController.systemStore.subscribe(m => this.forceUpdate());
        this.localStoreUnlistener = storageController.localStore.subscribe(m => this.forceUpdate());
        this.globalStoreUnlistener = storageController.globalStore.subscribe(m => this.forceUpdate());
        this.state = {
            controller: PageController.init(this.props)
        };
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            controller: PageController.init(nextProps, this.state.controller)
        });
    }
    componentWillUnmount() {
        if (this.systemUnlistener)
            this.systemUnlistener();
        if (this.localStoreUnlistener)
            this.localStoreUnlistener();
        if (this.globalStoreUnlistener)
            this.globalStoreUnlistener();
    }
    render() {
        const controller = this.state.controller;
        const ComponentClass = this.props.component;
        const component = <ComponentClass controller={controller}/>;
        const notUsedAreaView = getNotUsedAreaView(ComponentClass);
        return notUsedAreaView ? (component) : (<AreaView controller={controller}>{component}</AreaView>);
    }
}
