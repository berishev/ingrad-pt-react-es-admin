import * as React from 'react';
import { Route } from 'react-router-dom';
import PermissionRoute from './permissionRoute';
import { parseQueryString } from '../util/parsePath';
export default class DataRoute extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        let { component, data, convertParams } = this.props;
        let path = ((component && component['routePath']) ||
            this.props.path);
        return (<Route {...this.props} path={path} component={undefined} render={props => {
            let search = props.location.search;
            let paramsString = search && search.substring(1);
            let params = parseQueryString(paramsString);
            return (<PermissionRoute {...this.props} {...props} path={path} params={params}/>);
        }}/>);
    }
}
