import * as React from 'react';
import { withRouter } from 'react-router';
import ContentRoute from './contentRoute';
export class WithRouteComponent extends React.Component {
    render() {
        let params = this.props.params || this.props.match.params || {};
        if (this.props.data != null)
            params = Object.assign(params, this.props.data);
        if (this.props.convertParams != null)
            params = this.props.convertParams(params);
        let component = this.props.component;
        let pureComponent = this.props.component;
        return (<ContentRoute component={component} params={params} history={this.props.history}/>);
    }
}
export default withRouter(WithRouteComponent);
