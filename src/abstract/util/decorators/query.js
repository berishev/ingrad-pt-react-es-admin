import { promiseGet } from 'berish-promise-getter';
export class RouteQuery {
    constructor(params) {
        this._meta = null;
        this._condition = null;
        this._condition = params.condition;
        this._meta = params.meta || {};
    }
    getMeta() {
        return this._meta || {};
    }
    static or(...queries) {
        let promise = () => new Promise(async (resolve, reject) => {
            for (let query of queries) {
                let response = await query.execute();
                let { result, meta } = response;
                if (result) {
                    resolve(true);
                }
            }
            resolve(false);
        });
        let meta = {};
        for (let query of queries) {
            meta = Object.assign({}, meta, query.getMeta());
        }
        return new RouteQuery({ condition: promise, meta });
    }
    static and(...queries) {
        let promise = () => new Promise(async (resolve, reject) => {
            for (let query of queries) {
                let response = await query.execute();
                let { result, meta } = response;
                if (!result) {
                    resolve(false);
                }
            }
            resolve(true);
        });
        let meta = {};
        for (let query of queries) {
            meta = Object.assign({}, meta, query.getMeta());
        }
        return new RouteQuery({ condition: promise, meta });
    }
    static condition(value) {
        return new RouteQuery({ condition: value });
    }
    meta(meta) {
        this._meta = Object.assign({}, this._meta, meta);
        return this;
    }
    async execute() {
        let result = await promiseGet(this._condition);
        let meta = this.getMeta();
        return {
            result,
            meta
        };
    }
}
