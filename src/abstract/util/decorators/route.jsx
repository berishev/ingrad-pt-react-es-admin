import { getCurrentUser, getCurrentRole } from '../auth';
import { RouteQuery } from './query';
import { promiseGet } from 'berish-promise-getter/dist';
const ROUTEPATH_KEY = 'routePath';
const NOTUSEDAREAVIEW_KEY = 'notUsedAreaView';
const ROUTEQUERY_KEY = 'routeQuery';
export function RoutePath(route) {
    return function (Component) {
        Component[ROUTEPATH_KEY] = route;
        return Component;
    };
}
export function DNotUsedAreaView(Component) {
    Component[NOTUSEDAREAVIEW_KEY] = true;
    return Component;
}
export function DRouteQuery(query) {
    return function (Component) {
        Component[ROUTEQUERY_KEY] = query;
        return Component;
    };
}
export function DUserRoute(cb, redirect) {
    let promise = () => new Promise(async (resolve) => {
        let user = await getCurrentUser();
        let result = await promiseGet(cb(user));
        resolve(result);
    });
    return new RouteQuery({
        condition: promise,
        meta: { redirect }
    });
}
export function DRoleRoute(cb, redirect) {
    let promise = () => new Promise(async (resolve) => {
        let role = await getCurrentRole();
        let result = await promiseGet(cb(role));
        return result;
    });
    return new RouteQuery({
        condition: promise,
        meta: { redirect }
    });
}
export function DPublicRoute() {
    return new RouteQuery({
        condition: true
    });
}
export function DNotAuthorized() {
    return DUserRoute(m => m == null, '/');
}
export function getNotUsedAreaView(Component) {
    return ((Component && Component[NOTUSEDAREAVIEW_KEY]) || null);
}
export function getRouteQuery(Component) {
    return ((Component && Component[ROUTEQUERY_KEY]) || null);
}
export function getRoutePath(Component) {
    return ((Component && Component[ROUTEPATH_KEY]) || null);
}
