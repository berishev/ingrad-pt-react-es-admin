import * as Parse from 'parse';
import * as collection from 'berish-collection';
import Decorators from 'berish-decorate';
import Enum from 'berish-enum';
import { IsRequired } from '../abstract/util/decorators/filter';
export const ProjectRelTypeEnum = Enum.createFromPrimitive('M', 'MO');
export const ProjectRelTypeLabels = new collection.Dictionary(new collection.KeyValuePair(ProjectRelTypeEnum.M, 'Москва'), new collection.KeyValuePair(ProjectRelTypeEnum.MO, 'Московская область'));
export class Project extends Parse.Object {
    constructor() {
        super('Project');
    }
    get code() {
        return this.get('code');
    }
    set code(value) {
        this.set('code', value);
    }
    get name() {
        return this.get('name');
    }
    set name(value) {
        this.set('name', value);
    }
    get commercialName() {
        return this.get('commercialName');
    }
    set commercialName(value) {
        this.set('commercialName', value);
    }
    get iconUrl() {
        return this.get('iconUrl');
    }
    set iconUrl(value) {
        this.set('iconUrl', value);
    }
    get relType() {
        return ProjectRelTypeEnum[this.get('relType')];
    }
    set relType(value) {
        this.set('relType', ProjectRelTypeEnum[value]);
    }
}
Decorators.methodDecorate(Project, 'code', [IsRequired()]);
Decorators.methodDecorate(Project, 'name', [IsRequired()]);
Decorators.methodDecorate(Project, 'iconUrl', [IsRequired()]);
Decorators.methodDecorate(Project, 'relType', [IsRequired()]);
Parse.Object.registerSubclass('Project', Project);
