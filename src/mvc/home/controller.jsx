import * as React from 'react';
import { messagesController } from '../../abstract/global';
import executeController from '../../abstract/global/executeController';
export default class MainViewController extends React.Component {
    constructor(props) {
        super(props);
        this.load = async () => {
            this.pushList();
        };
        this.pushList = () => {
            this.props.controller.navigator.push('/teamuser/list');
        };
    }
    componentWillMount() {
        executeController.tryLoad(this.load);
    }
    render() {
        return messagesController.loadingGlobal(true);
    }
}
