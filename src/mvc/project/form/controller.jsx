import * as React from 'react';
import * as Parse from 'parse';
import * as Model from '../../../model';
import View from './view';
import executeController from '../../../abstract/global/executeController';
export default class Controller extends React.Component {
    constructor(props) {
        super(props);
        this.load = async () => {
            let { id } = this.props.controller.navigator.params;
            let { item } = this.state;
            if (id)
                item = await new Parse.Query(Model.Project).get(id);
            this.setState({ item });
        };
        this.save = async () => {
            let { item } = this.state;
            item = await item.save();
            this.setState({ item });
            if (this.props.modal)
                return this.props.modal.resolve(item);
            else
                return this.props.controller.navigator.replace(`/team/form`, {
                    id: item.id
                });
        };
        this.cancel = () => {
            if (this.props.modal)
                return this.props.modal.reject();
            else
                return this.props.controller.navigator.replace(`/team/list`);
        };
        // VIEW
        this.onChange = (item) => this.setState({ item });
        this.onSave = () => executeController.tryLoadNotification(this.save);
        this.onCancel = () => executeController.tryLoad(this.cancel);
        this.state = {
            item: new Model.Project()
        };
    }
    componentDidMount() {
        executeController.tryLoad(this.load);
    }
    render() {
        return <View controller={this}/>;
    }
}
