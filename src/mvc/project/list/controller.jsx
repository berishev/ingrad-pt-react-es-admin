import * as React from 'react';
import * as Parse from 'parse';
import * as Model from '../../../model';
import Form from '../form/controller';
import View from './view';
import executeController from '../../../abstract/global/executeController';
import storageController from '../../../abstract/global/storageController';
export default class Controller extends React.Component {
    constructor(props) {
        super(props);
        this.getQuery = (query) => {
            query = query || new Parse.Query(Model.Project);
            return query;
        };
        this.load = async () => {
            const systemStore = storageController.systemStore;
            await systemStore.dispatch(systemStore.createMethod(m => {
                m.title = 'Проекты';
            }));
            let { query } = this.state;
            query = this.getQuery();
            this.setState({ query });
        };
        this.remove = async () => {
            let { selected } = this.state;
            await Promise.all(selected.map(m => m.destroy()));
        };
        // VIEW
        this.onSelect = (selected) => this.setState({ selected });
        this.onAdd = async () => {
            let item = await this.props.controller.navigator.pushModal(Form)();
            if (item)
                this.setState({ query: this.getQuery() });
        };
        this.onEdit = async () => {
            let item = await this.props.controller.navigator.pushModal(Form)({
                id: this.state.selected[0].id
            });
            if (item)
                this.setState({ query: this.getQuery() });
        };
        this.onRemove = async () => {
            await executeController.tryLoad(this.remove);
            this.setState({ query: this.getQuery(), selected: [] });
        };
        this.state = {
            query: this.getQuery(),
            selected: []
        };
    }
    componentDidMount() {
        executeController.tryLoad(this.load);
    }
    render() {
        return <View controller={this}/>;
    }
}
