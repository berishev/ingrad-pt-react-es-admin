import * as React from 'react';
import * as Model from '../../../model';
import Form from '../form/controller';
import View from './view';
import storageController from '../../../abstract/global/storageController';
import executeController from '../../../abstract/global/executeController';
export default class Controller extends React.Component {
    constructor(props) {
        super(props);
        this.load = async () => {
            const systemStore = storageController.systemStore;
            await systemStore.dispatch(systemStore.createMethod(m => {
                m.title = 'Отделы';
            }));
            let { query } = this.state;
            query = Model.Team.getQuery();
            this.setState({ query });
        };
        this.remove = async () => {
            let { selected } = this.state;
            await Promise.all(selected.map(m => m.destroy()));
        };
        // VIEW
        this.onSelect = (selected) => this.setState({ selected });
        this.onAdd = async () => {
            let item = await this.props.controller.navigator.pushModal(Form)();
            if (item)
                this.setState({ query: Model.Team.getQuery() });
        };
        this.onEdit = async () => {
            let item = await this.props.controller.navigator.pushModal(Form)({
                id: this.state.selected[0].id
            });
            console.log(item);
            if (item)
                this.setState({ query: Model.Team.getQuery() });
        };
        this.onRemove = async () => {
            await executeController.tryLoad(this.remove);
            this.setState({ query: Model.Team.getQuery(), selected: [] });
        };
        this.state = {
            query: Model.Team.getQuery(),
            selected: []
        };
    }
    componentDidMount() {
        executeController.tryLoad(this.load);
    }
    render() {
        return <View controller={this}/>;
    }
}
